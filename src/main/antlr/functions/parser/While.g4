/**
 * Grammar for the while language
 * @author Connor Ford
 */
grammar While;

start
  : program EOF;

program
  : stmt;

stmt
  : '(' stmt ')'                        # stmtbracket
  | var ':=' aexp                       # assign
  | 'skip'                              # skip
  | <assoc=right> stmt ';' stmt                       # comp
  | 'if' bexp  'then' '(' stmt ')' 'else' '(' stmt ')'   # if
  | 'while' bexp 'do' '(' stmt ')'              # while
  ;

bexp
  : '(' bexp ')'                        # bexpbracket
  | 'true'                              # true
  | 'false'                             # false
  | aexp '=' aexp                       # equals
  | aexp '<=' aexp                      # leq
  | '¬'bexp                             # not
  | bexp '^' bexp                       # and
  ;
aexp
  : '(' aexp ')'                        # axepbracket
  | aexp '*' aexp                       # multiply
  | aexp '+' aexp                       # add
  | aexp '-' aexp                       # subtract
  | NUM                                 # numeral
  | var                                 # variable
  ;

var
  : CHARVAR   # charvar
  | CHARNUMVAR  # charnumvar
  ;

CHARVAR
  : [a-z];

CHARNUMVAR
  : [a-z] NUM;

NUM
  : [0-9]+;

WS: [ \t\r\n]+ -> skip;
