/**
 * for input edit and save of while programs,
 * and access to program features.
 * Essentially a very small plaintext editor that
 * can parse its contents and pass the parse tree to other functions.
 */

package ui;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.math.BigInteger;
import javax.swing.*;
import javax.swing.filechooser.*;
import javax.swing.border.*;
import model.whiletree.WhileProgram;
import model.whiletree.Stmt;
import ui.interpreter.*;
import functions.interpreter.StateTable;
import functions.interpreter.SemanticInterpreter;

public class ProgramEditor extends FunctionGUI {
  private static final long serialVersionUID = 343L;

  private final JTextArea jtaCodeArea = new JTextArea("", 0, 80);

  private final JTextField jtfGodelNum = new JTextField(5);

  private final JTextPane jtpCode = new JTextPane();

  private final JButton jbLoad = new JButton("Load"),
                        jbSave = new JButton("Save"),
                        jbProofs = new JButton("Proove"),
                        jbGodelEncode = new JButton("Encode"),
                        jbGodelDecode = new JButton("Decode"),
                        jbInterpreter = new JButton("Interpret"),
                        jbQuit = new JButton("Quit");

  private final JFileChooser fileChooser = new JFileChooser();

  private IntegerInputDialog godelNumInput;

  public ProgramEditor() {
    setLayout(new BorderLayout());

    JPanel jpCodeView = new JPanel(new GridLayout(1,1,10,10));

    JToolBar jtbControls = new JToolBar();
    add(jtbControls, BorderLayout.PAGE_START);

    add(jpCodeView, BorderLayout.CENTER);

    jtaCodeArea.setFont(new Font("Monospaced", Font.PLAIN, 24));

    jpCodeView.add(jtaCodeArea);

    jtbControls.add(jbLoad);
    jtbControls.add(jbSave);
    jtbControls.addSeparator();
    jtbControls.add(jbInterpreter);
    jtbControls.add(jbProofs);
    jtbControls.add(jbGodelEncode);
    jtbControls.add(jbGodelDecode);

    jbLoad.addActionListener(this);
    jbSave.addActionListener(this);
    jbProofs.addActionListener(this);
    jbGodelEncode.addActionListener(this);
    jbGodelDecode.addActionListener(this);
    jbInterpreter.addActionListener(this);
    jbQuit.addActionListener(this);

    godelNumInput = new IntegerInputDialog(this);

    jtaCodeArea.setText("x := 2; while x <= 20 do (x := x*x)");
  }

  public void createAndShow() {
    JFrame frame = new JFrame("Code editor");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    frame.add(new ProgramEditor());

    frame.pack();

    frame.setSize(600, 750);
    frame.setVisible(true);
  }

  public WhileProgram getWhileProgramFromTextArea()
      throws Exception {
    WhileProgram prog = new WhileProgram(jtaCodeArea.getText());
    return prog;
  }

  public void readFileIntoCodeArea(File file) {
    //load the file contents into the editor textarea
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(file));
      jtaCodeArea.setText("");
      String currLine;
      while ((currLine = br.readLine()) != null) {
        jtaCodeArea.append(currLine + '\n');
      }
    } catch (IOException ex) {
      showDialog("Exception reading file: " + ex.getMessage());
    } finally {
      try {
        if (br != null) {
          br.close();
        }
      } catch (IOException ex) {
        showDialog("Exception closing file reader: " + ex.getMessage());
      }
    }
  }

  public void writeToFileFromCodeArea(File file) {
    PrintWriter pw = null;
    try {
      pw = new PrintWriter(new FileWriter(file, false));
      pw.print(jtaCodeArea.getText());
    } catch (IOException ex) {
      showDialog("Exception writing to file: " + ex.getMessage());
    } finally {
      pw.close();
      if (pw.checkError()) {
        showDialog("Exception closing file writer");
      }
    }
  }

  public void showDialog(String s) {
    JOptionPane.showMessageDialog(this, s);
  }

  public void actionPerformed(ActionEvent e) {

    if (e.getSource() == jbLoad) {
      int returnVal = fileChooser.showOpenDialog(this);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        readFileIntoCodeArea(fileChooser.getSelectedFile());
      } else {
        showDialog("loading cancelled");
      }
    } else if (e.getSource() == jbSave) {
      int returnVal = fileChooser.showSaveDialog(this);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        writeToFileFromCodeArea(fileChooser.getSelectedFile());
      } else {
        showDialog("saving cancelled");
      }
    } else if (e.getSource() == jbInterpreter) {
      try {
        WhileProgram prog = getWhileProgramFromTextArea();
        StateTable initVals;
        if (prog.getVariables().length > 0) {
          InitialValuePanel jpInitVal
                                  = new InitialValuePanel(prog.getVariables());
          JOptionPane.showConfirmDialog(this,
                                        jpInitVal,
                                        "Enter initial values.",
                                        JOptionPane.DEFAULT_OPTION);

          initVals = jpInitVal.getInitialValues();
        } else {
          initVals = new StateTable();
        }
        new Interpreter(new SemanticInterpreter(prog, initVals))
                                               .createAndShow();
      } catch (Exception ex) {
        showDialog("Exception running interpreter function: "
                   + ex.getMessage());
      }
    } else if (e.getSource() == jbQuit) {
      System.exit(0);
    }
  }
}
