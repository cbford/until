package ui;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import model.whiletree.WhileProgram;

public abstract class FunctionGUI extends JPanel implements ActionListener {
  private WhileProgram program;

  private static final long serialVersionUID = 343L;

  //method takes a valid WhileProgram from the program editor
  //so that we can apply the function to it
  public abstract void createAndShow()
      throws Exception;
}
