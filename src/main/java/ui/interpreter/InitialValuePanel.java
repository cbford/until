package ui.interpreter;

import javax.swing.*;
import java.awt.*;
import functions.interpreter.StateTable;
import model.whiletree.WhileProgram;
import model.whiletree.Variable;

public class InitialValuePanel extends JPanel {
  private static final long serialVersionUID = 343L;
  private Variable[] vars;
  private JTextField[] jtfInputs;

  public InitialValuePanel(Variable[] vs) {
    super(new GridLayout(0,1));

    vars = vs;
    int nVars = vars.length;
    jtfInputs = new JTextField[nVars];

    //lay it out
    JPanel row;
    for (int i = 0; i < nVars; i++) {
      jtfInputs[i] = new JTextField("0", 3);

      row = new JPanel();
      row.add(new JLabel(vars[i].toString()));
      row.add(jtfInputs[i]);
      add(row);
    }  
  }


  public StateTable getInitialValues()
      throws Exception {
    try {
      StateTable vals = new StateTable();
      int val;

      for (int i = 0; i < vars.length; i++) {
        val = Integer.parseInt(jtfInputs[i].getText());
        vals.update(vars[i], val);
      }

      return vals;
    } catch (NumberFormatException nex) {
      throw new Exception("Each variable must be given"
                          + "an intger initial value");
    }
  }
}
