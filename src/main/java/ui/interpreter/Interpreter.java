package ui.interpreter;

import java.util.Stack;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.filechooser.*;
import model.whiletree.WhileProgram;
import model.whiletree.EmptyStmt;
import functions.interpreter.*;

public class Interpreter extends ui.FunctionGUI {
  private static final long serialVersionUID = 343L;

  JButton jbStepNForwards = new JButton("(100) >>"),
          jbStepForwards = new JButton(">"),
          jbStepBackwards = new JButton("<");

  JScrollPane jspInterpretation;

  JTable table;
  InterpreterTableModel tableModel;

  SemanticInterpreter semanticInterpreter;

  Stack<SemanticPair> history; //what has been viewed

  private WhileProgram program;

  public Interpreter(SemanticInterpreter si) {
    semanticInterpreter = si;
    program = si.getProgToInterpret();

    tableModel = new InterpreterTableModel(si);
    table = new JTable(tableModel);
    add(table);

    this.setLayout(new BorderLayout(10,10));

    JToolBar jtbControls = new JToolBar();
    add(jtbControls, BorderLayout.PAGE_START);

    jspInterpretation = new JScrollPane(table);

    this.add(jspInterpretation, BorderLayout.CENTER);

    jtbControls.add(jbStepForwards);
    jtbControls.add(jbStepBackwards);
    jtbControls.addSeparator();
    jtbControls.add(jbStepNForwards);

    jbStepBackwards.setEnabled(false);

    jbStepNForwards.addActionListener(this);
    jbStepForwards.addActionListener(this);
    jbStepBackwards.addActionListener(this);

    history = new Stack<SemanticPair>();
  }

  //make sure the whole code block is visible
  private void resizeRowForCode(int row) {
    int rowsNeeded = semanticInterpreter.getExecutedWhileProgramAtPosition(row)
                                          .toString().split("\n").length;
    //the resize factor depends on the screen resolution
    //  40 has been chosen since it should be enough for most monitors
    table.setRowHeight(row, rowsNeeded * 50); 
  }

  //applied once to make sure the initial code can be displayed
  private void resizeColumnForCode() {
    int colsNeeded = 0;
    for (String line : semanticInterpreter
                        .getProgToInterpret().toString().split("\n")) {
      if (line.length() > colsNeeded) {
        colsNeeded = line.length();
      }
    }

    table.getColumnModel().getColumn(0).setPreferredWidth(colsNeeded * 10);
  }

  public void createAndShow() {
    JFrame frame = new JFrame("Interpreter");

    frame.add(this);
    resizeRowForCode(0);
    resizeColumnForCode();
    frame.pack();
    frame.setVisible(true);
  }

  private void updateButtons() {
    jbStepForwards.setEnabled(semanticInterpreter.canStepForwards());
    jbStepNForwards.setEnabled(semanticInterpreter.canStepForwards());
    jbStepBackwards.setEnabled(semanticInterpreter.canStepBackwards());
  }

  private void stepForwards() {
    if (semanticInterpreter.canStepForwards()) {
      semanticInterpreter.stepForwards();
      tableModel.nextRow();
      resizeRowForCode(table.getRowCount() - 1);
    }
  }

  private void stepBackwards() {
    if (semanticInterpreter.canStepBackwards()) {
      semanticInterpreter.stepBackwards();
      tableModel.removeLastRow();
      resizeRowForCode(table.getRowCount() - 1);
    }
  }

  public void actionPerformed(ActionEvent e) {
    //act on the model
    if (e.getSource() == jbStepNForwards) {
      for (int i = 0; i < 100; i++) {
        stepForwards();
      }
    } else if (e.getSource() == jbStepForwards) {
      stepForwards();
    } else if (e.getSource() == jbStepBackwards) {
      stepBackwards();
    }

    updateButtons();

    //scroll to bottom of the pane
    int height = (int)table.getPreferredSize().getHeight();
    Rectangle rect = new Rectangle(0,height,10,10);
    table.scrollRectToVisible(rect);
  }
}
