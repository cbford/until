package ui.interpreter;

import functions.interpreter.SemanticInterpreter;

import javax.swing.table.AbstractTableModel;

public class InterpreterTableModel extends AbstractTableModel {
  private static final long serialVersionUID = 343L;
  private static final String[] columnNames = {"Code", "State", "Rule applied"};
  private SemanticInterpreter interpreter;

  public InterpreterTableModel(SemanticInterpreter intrp) {
    interpreter = intrp;
    nextRow();
  }

  public String getColumnName(int col) {
    return columnNames[col];
  }

  public int getRowCount() {
    return interpreter.getCurrPairPosition() + 1;
  }

  public int getColumnCount() {
    return 3;
  }

  public Object getValueAt(int row, int col) {
    switch (col) {
      case 0:
        //formatting to keep linebreaks and indents in table cell
        return "<html><pre>" + interpreter.getExecutedWhileProgramAtPosition(row)
                            .toString().replaceAll(">", "&gt;")
                                       .replaceAll("<", "&lt;")
                                       .replaceAll("\n", "<br>")
                             + "</pre></html>";
      case 1:
        return interpreter.getStateTableAtPosition(row);
      case 2:
        return interpreter.getRuleAppliedAtPosition(row);
      default:
        //cannot happen
        throw new RuntimeException("Tried to access out of bounds column");
    }
  }

  public boolean isCellEditable(int row, int col) {
    return false;
  }

  public void setValueAt(Object value, int row, int col) {
    fireTableRowsInserted(row, row);
  }

  public void nextRow() {
    int pos = interpreter.getCurrPairPosition();
    setValueAt(interpreter.getExecutedWhileProgram(), pos,0);
    setValueAt(interpreter.getStateTable(), pos,1);
    setValueAt(interpreter.getRuleApplied(), pos,2);
  }

  public void removeLastRow() {
    fireTableRowsDeleted(getRowCount() - 1, getRowCount() - 1);
  }

}
