package ui;

import javax.swing.*;
import java.awt.*;

public class IntegerInputDialog extends InputDialog<Integer> {
  public IntegerInputDialog(JPanel p) {
    super(p);
  }

  public Integer makeObjectFromInputText()
      throws IllegalArgumentException {
    try {
      return Integer.parseInt(jtfInput.getText());
    } catch (NumberFormatException ex) {
      throw new IllegalArgumentException("Couldn't treat input as an integer");
    } catch (Exception ex) {
      throw new IllegalArgumentException(ex.getMessage());
    }

  }

}
