package ui;

import javax.swing.*;
import java.awt.*;

//T is the returned object
public abstract class InputDialog<T> extends JOptionPane {
  private static final long serialVersionUID = 343L;
  protected JTextField jtfInput = new JTextField(15);
  private JLabel jlPromptMessage = new JLabel();
  private String message;
  private T objectFromInput;
  private JPanel owner, //needed to request focus
                 dialogBody;

  public InputDialog() {}

  public InputDialog(JPanel p) {
    owner = p;
    dialogBody = new JPanel(new BorderLayout());
    dialogBody.add(jlPromptMessage, BorderLayout.NORTH);
    dialogBody.add(jtfInput, BorderLayout.CENTER);
  }

  public void requestInputFromUser(String s) {
    message = s;
    jlPromptMessage.setText(message);
    JOptionPane.showConfirmDialog(owner,
                                  dialogBody,
                                  "Input request",
                                  JOptionPane.DEFAULT_OPTION);
    jtfInput.requestFocus();
  }

  public abstract T makeObjectFromInputText()
      throws IllegalArgumentException;

  public T getInputValue() {
    try {
      T val = makeObjectFromInputText();
      jtfInput.setText("");
      return val;
    } catch (IllegalArgumentException ex) {
      JOptionPane.showMessageDialog(this, "Invalid input:\n" + ex.getMessage());

      requestInputFromUser(message);
      return getInputValue();
    }
  }
}
