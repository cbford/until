package model.whiletree;

import functions.interpreter.StateTable;

public class Subtract extends AExpr {
  private AExpr a1, a2;

  public Subtract(AExpr first, AExpr second) {
    a1 = first;
    a2 = second;

    a1.parent = this;
    a2.parent = this;
  }

  public int evaluate(StateTable s) {
    return a1.evaluate(s) - a2.evaluate(s);
  }

  public String toString() {
    return a1 + "-" + a2;
  }

  public AExpr getFirst() {
    return a1;
  }

  public AExpr getSecond() {
    return a2;
  }
}
