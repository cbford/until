package model.whiletree;

import functions.interpreter.SemanticPair;
import functions.interpreter.StateTable;

public class Skip extends Stmt {
  public SemanticPair getSemanticInterpretation(StateTable s) {
    return new SemanticPair(new EmptyStmt(), s, "Skip");
  }

  public String toString() {
    return "skip";
  }
}
