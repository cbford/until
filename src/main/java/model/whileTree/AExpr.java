package model.whiletree;

import functions.interpreter.StateTable;

public abstract class AExpr extends WhileNode {

  public abstract String toString();
  
  public abstract int evaluate(StateTable s);
}
