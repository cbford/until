package model.whiletree;

import functions.interpreter.StateTable;

public class F extends BExpr {
  public boolean evaluate(StateTable s) {
    return false;
  }

  public String toString() {
    return "false";
  }
}
