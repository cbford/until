package model.whiletree;

import functions.interpreter.StateTable;

public class Variable extends AExpr {
  protected char name;

  public Variable(char c) {
    name = c;
  }

  public int evaluate(StateTable s) {
    return s.retrieve(this);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Variable) {
      return this.hashCode() == ((Variable)o).hashCode();
    }
    else {
      return false;
    }
  }

  @Override
  public String toString() {
    return name + "";
  }

  @Override
  public int hashCode() {
    return name;
  }
}
