package model.whiletree;

import functions.interpreter.StateTable;

public abstract class BExpr extends WhileNode {
  public abstract String toString();
  public abstract boolean evaluate(StateTable s);
}
