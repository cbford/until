package model.whiletree;

import functions.interpreter.StateTable;
import functions.interpreter.SemanticPair;

public class EmptyStmt extends Stmt  {
  public void interpret() {}

  public SemanticPair getSemanticInterpretation(StateTable s) {
    return new SemanticPair(this, s);
  }

  public String toString()  {
    return "";
  }
}
