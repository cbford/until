package model.whiletree;

import functions.interpreter.StateTable;

public class Numeral extends AExpr {
  private int numericalValue;

  public Numeral(int v) {
    numericalValue = v;
  }

  public int evaluate(StateTable s) {
    return numericalValue;
  }

  @Override
  public String toString() {
    return numericalValue + "";
  }
}
