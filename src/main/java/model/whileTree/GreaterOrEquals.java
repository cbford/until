//extened while syntax construct

package model.whiletree;

import functions.interpreter.StateTable;

public class GreaterOrEquals extends BExpr {
  private AExpr lhs, rhs;
  
  public GreaterOrEquals(AExpr l, AExpr r) {
    lhs = l;
    rhs = r;

    lhs.parent = this;
    rhs.parent = this;
  }

  public boolean evaluate(StateTable s) {
    return lhs.evaluate(s) >= rhs.evaluate(s);
  }

  public String toString() {
    return lhs + ">=" + rhs;
  }

  public AExpr getFirst() {
    return lhs;
  }

  public AExpr getSecond() {
    return rhs;
  }
}
