package model.whiletree;

import functions.interpreter.StateTable;
import functions.interpreter.SemanticPair;

public class If extends Stmt {
  private BExpr condition;
  private Stmt whenTrue, whenFalse;

  public If(BExpr b, Stmt t, Stmt f) {
    condition = b;
    whenTrue = t;
    whenFalse = f;

    condition.parent = this;
    whenTrue.parent = this;
    whenFalse.parent = this;
  }

  public SemanticPair getSemanticInterpretation(StateTable s) {
    Stmt consequence;
    boolean conditionIsTrue = condition.evaluate(s);
    String ruleApplied = "If " + ((conditionIsTrue) ? "(tt)" : "(ff)");

    if (conditionIsTrue) {
      consequence = whenTrue;
    } else {
      consequence = whenFalse;
    }

    return new SemanticPair(consequence, s, ruleApplied);
  }

  public BExpr getCondition() {
    return condition;
  }

  public Stmt getWhenTrue() {
    return whenTrue;
  }

  public Stmt getWhenFalse() {
    return whenFalse;
  }

  public String toString() {
    return "if " + condition + " then (\n"
           + whenTrue.getIndentedString()
           + ") else (\n"
           + whenFalse.getIndentedString() + ")";
  }
}
