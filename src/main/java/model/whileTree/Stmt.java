package model.whiletree;

import functions.interpreter.SemanticPair;
import functions.interpreter.StateTable;

public abstract class Stmt extends WhileNode {

  public abstract String toString();

  public abstract SemanticPair getSemanticInterpretation(StateTable s);

  public Stmt copyStmt() {
    try {
      return new WhileProgram(this.toString()).getFirstStmt();
    } catch (Exception e) {
      System.err.println("error copying a stmt, returning empty. stmt was "
                         + this.toString());
      return new EmptyStmt();
    }
  }

  protected String getIndentedString() {
    String toIndent = toString(),
           indented = "";
    //add indent to beginning of each line in the string
    for (String line : toIndent.split("\n")) {
      indented += "  " + line + "\n";
    }

    return indented;
  }

  public String toOneLineString() {
    return toString().replace('\n', ' ');
  }

  public String toLatex() {
    return toOneLineString().replace("if", " \\If ")
                            .replace("then", "\\Then")
                            .replace("else", "\\Else")
                            .replace("skip", "\\Skip")
                            .replace("while", "\\While")
                            .replace("do", "\\Do")
                            .replace("true", "\\True")
                            .replace("false", "\\False")
                            .replace("for", "\\For")
                            .replace("to", "\\To")
                            .replace(":=", " \\Ass ")
                            .replace("<=", " \\leq ")
                            .replace("<", " \\le ")
                            .replace("  ", " ")
                            .replace(" ", "~")
                            .replace("~~", "~");
  }
}
