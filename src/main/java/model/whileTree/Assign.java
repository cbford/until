package model.whiletree;

import functions.interpreter.StateTable;
import functions.interpreter.SemanticPair;

public class Assign extends Stmt {
  private Variable assignedTo;
  private AExpr assignFrom;

  public Assign(Variable v, AExpr e) {
    assignedTo = v;
    assignFrom = e;

    assignedTo.parent = this;
    assignFrom.parent = this;
  }

  public SemanticPair getSemanticInterpretation(StateTable s) {
    //update s with result of assignment
    SemanticPair result = new SemanticPair(new EmptyStmt(), s, "Assign");
    result.getStateTable().update(assignedTo, assignFrom.evaluate(s));
    return result; 
  }

  public AExpr getAssignFrom() {
    return assignFrom;
  }

  public Variable getAssignTo() {
    return assignedTo;
  }

  public String toString() {
    return assignedTo + ":=" + assignFrom;
  }
}
