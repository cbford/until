/**
 * A while program as a tree that can be interpreted by traversal
 */

package model.whiletree;

import functions.parser.WhileCodeVisitor;
import java.util.HashSet;
import main.App;

public class WhileProgram extends WhileNode {
  //the program represented as a tree
  WhileNode program;
  private HashSet<Variable> vars;

  //construct a program from a string representation
  public WhileProgram(String progAsString)
      throws Exception {
    parent = this;
    WhileCodeVisitor visitor = new WhileCodeVisitor();
    program = visitor.getWhileTree(progAsString);
    vars = visitor.getVariables();
    program.parent = this;
  }

  public Stmt getFirstStmt() {
    return (Stmt)program;
  }

  public String toString() {
    return program.toString();
  }

  public int hashCode() {
    return toString().replaceAll("\\s", "").hashCode();
  }

  public Variable[] getVariables() {
    return vars.toArray(new Variable[vars.size()]);
  }
}
