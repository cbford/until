package model.whiletree;

import functions.interpreter.StateTable;
import functions.interpreter.SemanticPair;

public class Comp extends Stmt {
  private Stmt s1, s2;

  public Comp(Stmt first, Stmt second) {
    s1 = first;
    s2 = second;

    s1.parent = this;
    s2.parent = this;
  }

  public SemanticPair getSemanticInterpretation(StateTable s) {
    SemanticPair subinterpretation = s1.getSemanticInterpretation(s);
    StateTable subState = subinterpretation.getStateTable();
    Stmt subStmt = subinterpretation.getProgram();
    String subRule = subinterpretation.getDerivedBy();

    if (subStmt instanceof EmptyStmt) {
      return new SemanticPair(s2, subState, subRule + ", Composition 2");
    } else {
      return new SemanticPair(new Comp(subStmt, s2),
                              subState,
                              subRule + ", Composition 1");
    }
  }

  public Stmt getFirst() {
    return s1;
  }

  public Stmt getSecond() {
    return s2;
  }

  public String toString() {
    return s1 + ";\n" + s2;
  }
}
