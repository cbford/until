package model.whiletree;

import functions.interpreter.StateTable;
import functions.interpreter.SemanticPair;

public class While extends Stmt {
  private BExpr condition;
  private Stmt loopBody;

  public While(BExpr b, Stmt s) {
    condition = b;
    loopBody = s;

    condition.parent = this;
    loopBody.parent = this;
  }

  public SemanticPair getSemanticInterpretation(StateTable s) {
    return new SemanticPair(new If(condition,
                                   new Comp(loopBody, this.copyStmt()),
                                   new Skip()),
                            s,
                            "While");
  }

  public BExpr getCondition() {
    return condition;
  }

  public Stmt getLoopBody() {
    return loopBody;
  }

  public String toString() {
    return "while " + condition + " do (\n"
           + loopBody.getIndentedString() + ")";
  }
}
