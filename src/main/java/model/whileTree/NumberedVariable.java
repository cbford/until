package model.whiletree;

import functions.interpreter.StateTable;

public class NumberedVariable extends Variable {
  private int number;

  public NumberedVariable(char c, int n) {
    super(c);
    number = n;
  }

  @Override
  public int hashCode() {
    return name+number;
  }

  @Override
  public String toString() {
    return name + "" + number;
  }
}
