package model.whiletree;

import functions.interpreter.StateTable;

public class Negate extends BExpr {
  private BExpr toNegate;

  public Negate(BExpr b) {
    toNegate = b;

    toNegate.parent = this;
  }

  public boolean evaluate(StateTable s) {
    return !toNegate.evaluate(s);
  }

  public String toString() {
    return "¬" + toNegate;
  }

  public BExpr getToNegate() {
    return toNegate;
  }
}
