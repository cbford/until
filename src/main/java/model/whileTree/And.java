package model.whiletree;

import functions.interpreter.StateTable;

public class And extends BExpr {
  private BExpr b1, b2;

  public And(BExpr first, BExpr second) {
    b1 = first;
    b2 = second;

    b1.parent = this;
    b2.parent = this;
  }
  public boolean evaluate(StateTable s) {
    return b1.evaluate(s) && b2.evaluate(s);
  }

  public String toString() {
    return b1 + "^" + b2;
  }

  public BExpr getFirst() {
    return b1;
  }

  public BExpr getSecond() {
    return b2;
  }
}
