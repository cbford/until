/**
 * A node in the intermediate code representation tree
 */

package model.whiletree;

public abstract class WhileNode {
  protected WhileNode parent;
}
