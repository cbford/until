package main;

import model.whiletree.WhileProgram;
import ui.ProgramEditor;

import java.awt.Font;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

public class App {
  private static boolean extendedGrammarEnabled = false;

  public static void main(String[] args) {
    setCommandLineArgs(args);
    new ProgramEditor().createAndShow();
  }

  //hidpi flag can be passed at command line to ensure ui font is not too small
  public static void setCommandLineArgs(String[] args) {
    for (String arg : args) {
      if (arg.equals("--hidpi")) {
        setFont(new FontUIResource("Sans-Serif",Font.PLAIN,24));
      } else if (arg.equals("--extendedGrammar")) {
        extendedGrammarEnabled = true;
      }
    }
  }
  
  public static boolean extendedGrammarIsEnabled() {
    return extendedGrammarEnabled;
  }

  //set the overall ui font
  public static void setFont(FontUIResource font) {
    java.util.Enumeration keys = UIManager.getDefaults().keys();
    while (keys.hasMoreElements()) {
      Object key = keys.nextElement();
      Object value = UIManager.get(key);
      if (value != null && value instanceof FontUIResource) {
        UIManager.put(key, font);
      }
    }
  }
}
