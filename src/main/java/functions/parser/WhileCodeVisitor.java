/**
 * Visit an ANTLR-generated AST and construct
 * the WhileNode intermediate representation of the program
 */

package functions.parser;

import model.whiletree.*;
import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import java.util.HashSet;

public class WhileCodeVisitor extends WhileBaseVisitor<WhileNode> {
  //we have custom defined error handling behaviour
  private final ANTLRErrorListener errorListener
                                          = new WhileCodeVisitorErrorListener();
  private HashSet<Variable> variables;

  /**
   * Returns a WhileNode tree intermerdiate representation
   * of the program as a given string
   *
   * @param codeAsString the program string to parse
   * @return the WhileNode tree representation
   */
  public WhileNode getWhileTree(String codeAsString)
        throws Exception {
    //the custom error throws exceptions when there is a syntax error
    WhileLexer lexer = new WhileLexer(
                          new ANTLRInputStream(codeAsString));

    //use my error listner implementation instead of default
    lexer.removeErrorListeners();
    lexer.addErrorListener(errorListener);
  
    CommonTokenStream tokens = new CommonTokenStream(lexer);

    //determine variables from tokens
    variables = new HashSet<Variable>();
    tokens.fill();
    for (Token t : tokens.getTokens()) {
      if (t.getType() == WhileLexer.CHARVAR) {
        variables.add(new Variable(t.getText().charAt(0)));
      }else if (t.getType() == WhileLexer.CHARNUMVAR) {
        variables.add(new NumberedVariable(t.getText().charAt(0),
                                   Integer.parseInt(t.getText().substring(1))));
      }
    }

    WhileParser parser = new WhileParser(tokens);
    //use my error listner implementation instead of default
    parser.removeErrorListeners();
    parser.addErrorListener(errorListener);

    
    //generate the ANTLR AST
    ParseTree tree = parser.start();
  
    //construct the WhileNode tree given the root of the AST
    return visit(tree.getChild(0));
  }

  public HashSet<Variable> getVariables() {
    return variables;
  }

  //Methods to visit different types of AST node and construct the WhileNode.
  // the child nodes are children in the AST
  // with type dependant on the production rule in the grammar

  @Override
  public WhileNode visitComp(WhileParser.CompContext ctx) {
    Stmt s1 = (Stmt)visit(ctx.stmt(0)),
         s2 = (Stmt)visit(ctx.stmt(1));

    return new Comp(s1, s2);
  }

  @Override
  public WhileNode visitSkip(WhileParser.SkipContext ctx) {
    return new Skip();
  }

  @Override
  public WhileNode visitWhile(WhileParser.WhileContext ctx) {
    BExpr condition = (BExpr)visit(ctx.bexp());
    Stmt loopBody = (Stmt)visit(ctx.stmt());

    return new While(condition, loopBody);
  }

  @Override
  public WhileNode visitIf(WhileParser.IfContext ctx) {
    BExpr condition = (BExpr)visit(ctx.bexp());
    Stmt whenTrue = (Stmt)visit(ctx.stmt(0)),
         whenFalse = (Stmt)visit(ctx.stmt(1));

    return new If(condition, whenTrue, whenFalse);
  }

  @Override
  public WhileNode visitStmtbracket(WhileParser.StmtbracketContext ctx) {
    return (Stmt)visit(ctx.stmt());
  }

  @Override
  public WhileNode visitAssign(WhileParser.AssignContext ctx) {
    Variable var = (Variable)visit(ctx.var());
    AExpr val = (AExpr)visit(ctx.aexp());

    return new Assign(var, val);
  }

  @Override
  public WhileNode visitNot(WhileParser.NotContext ctx) {
    BExpr toNeg = (BExpr)visit(ctx.bexp());

    return new Negate(toNeg);
  }

  @Override
  public WhileNode visitBexpbracket(WhileParser.BexpbracketContext ctx) {
    return (BExpr)visit(ctx.bexp());
  }

  @Override
  public WhileNode visitAnd(WhileParser.AndContext ctx) {
    BExpr c1 = (BExpr)visit(ctx.bexp(0)),
          c2 = (BExpr)visit(ctx.bexp(1));

    return new And(c1, c2);
  }

  @Override
  public WhileNode visitEquals(WhileParser.EqualsContext ctx) {
    AExpr lhs = (AExpr)visit(ctx.aexp(0)),
          rhs = (AExpr)visit(ctx.aexp(1));

    return new Equals(lhs, rhs);
  }

  @Override
  public WhileNode visitTrue(WhileParser.TrueContext ctx) {
    return new T();
  }

  @Override
  public WhileNode visitFalse(WhileParser.FalseContext ctx) {
    return new F();
  }

  @Override
  public WhileNode visitLeq(WhileParser.LeqContext ctx) {
    AExpr lhs = (AExpr)visit(ctx.aexp(0)),
          rhs = (AExpr)visit(ctx.aexp(1));

    return new LessOrEquals(lhs, rhs);
  }

  @Override
  public WhileNode visitAdd(WhileParser.AddContext ctx) {
    AExpr lhs = (AExpr)visit(ctx.aexp(0)),
          rhs = (AExpr)visit(ctx.aexp(1));

    return new Add(lhs, rhs);
  }

  @Override
  public WhileNode visitSubtract(WhileParser.SubtractContext ctx) {
    AExpr lhs = (AExpr)visit(ctx.aexp(0)),
          rhs = (AExpr)visit(ctx.aexp(1));

    return new Subtract(lhs, rhs);
  }

  @Override
  public WhileNode visitVariable(WhileParser.VariableContext ctx) {
    Variable var = (Variable)visit(ctx.var());
    return var;
  }

  @Override
  public WhileNode visitCharvar(WhileParser.CharvarContext ctx) {
    Variable var = new Variable(ctx.CHARVAR().toString().charAt(0));
    return var;
  }

  @Override
  public WhileNode visitCharnumvar(WhileParser.CharnumvarContext ctx) {
    Variable var = new NumberedVariable(ctx.CHARNUMVAR().toString().charAt(0),
                                        Integer.parseInt(ctx.CHARNUMVAR().toString().substring(1)) );
    return var;
  }

  @Override
  public WhileNode visitNumeral(WhileParser.NumeralContext ctx) {
    int num = 0;
    //read digit by digit
    for (int i = 0; i < ctx.getChildCount(); i++) {
      num = (num * 10) + Integer.parseInt(ctx.getChild(i).getText());
    }

    return new Numeral(num);
  }

  @Override
  public WhileNode visitMultiply(WhileParser.MultiplyContext ctx) {
    AExpr lhs = (AExpr)visit(ctx.aexp(0)),
          rhs = (AExpr)visit(ctx.aexp(1));

    return new Multiply(lhs, rhs);
  }

  @Override
  public WhileNode visitAxepbracket(WhileParser.AxepbracketContext ctx) {
    return (AExpr)visit(ctx.aexp());
  }
}
