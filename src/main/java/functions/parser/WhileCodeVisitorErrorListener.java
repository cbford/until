/**
 * Custom implementation of ANTLR error handling
 */

package functions.parser;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import java.util.Locale;
import java.util.BitSet;

public class WhileCodeVisitorErrorListener extends BaseErrorListener {
  //if we get a syntax error throw an exception stating where is was
  public void syntaxError(final Recognizer<?, ?> arg0,
                          final Object obj,
                          final int line,
                          final int position,
                          final String message,
                          final RecognitionException ex) {
                throw new IllegalArgumentException(String.format(Locale.ROOT,
                      "unexpected when parsing while program:\n"
                      + "'%s' on line %s, position %s",
                      message, line, position));
  }
}
