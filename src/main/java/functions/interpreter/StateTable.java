/**
 * Model the storage of variables in an instance of interpretation.
 * This encapsulates a hashtable of variable names mapped to their value.
 */

package functions.interpreter;

import model.whiletree.Variable;

import java.util.Hashtable;

public class StateTable {
  private Hashtable<Variable, Integer> variableStore;

  public StateTable() {
    variableStore = new Hashtable<Variable, Integer>();
  }

  /**
   * update a variable's value
   */
  public void update(Variable var, int val) {
    variableStore.put(var, val);
  }

  /**
   * return a variable's value
   * @param var the variable whose value to retrieve
   * @return the value in the table, or 0 if not in the table
   */
  public int retrieve(Variable var) {
    if (variableStore.keySet().contains(var)) {
      return variableStore.get(var);
    } else {
      System.out.println(var + " is an unknown variable, assuming 0");
      return 0;
    }
  }

  //copies values into this table
  public void updateFromOtherStateTable(StateTable otherTable) {
    variableStore.putAll(otherTable.variableStore);
  }

  public String toString() {
    if (variableStore.size() == 0) {
      return "[]";
    }

    String ret = "[";

    for (Variable v : variableStore.keySet()) {
      ret += v + " -> " + variableStore.get(v) + ", ";
    }

    return ret.substring(0, ret.length() - 2) + "]";
  }

  /**
   * @return true iff no updates to the table have been made
   */
  public boolean isEmpty() {
    return variableStore.isEmpty();
  }
}
