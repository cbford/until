package functions.interpreter;

import functions.interpreter.StateTable;
import model.whiletree.EmptyStmt;
import model.whiletree.Stmt;
import model.whiletree.WhileProgram;

import java.util.LinkedList;

public class SemanticInterpreter {
  WhileProgram progToInterpret;

  LinkedList<SemanticPair> pairHistory;
  LinkedList<String> ruleHistory;
  int currPairPosition = 0;

  public SemanticInterpreter(WhileProgram prog)
        throws Exception {
    progToInterpret = prog;

    SemanticPair firstPair = new SemanticPair(new WhileProgram(prog.toString())
                                                              .getFirstStmt(),
                                              new StateTable());

    pairHistory = new LinkedList<SemanticPair>();
    pairHistory.add(firstPair);
    ruleHistory = new LinkedList<String>();
    ruleHistory.add("initial state");
  }

  public SemanticInterpreter(WhileProgram prog, StateTable s)
        throws Exception {
    progToInterpret = prog;

    SemanticPair firstPair = new SemanticPair(new WhileProgram(prog.toString())
                                                              .getFirstStmt(),
                                              s);

    pairHistory = new LinkedList<SemanticPair>();
    pairHistory.add(firstPair);
    ruleHistory = new LinkedList<String>();
    ruleHistory.add("intial"); 
  }

  public void stepForwards() {
    if (currPairPosition < pairHistory.size() - 1) {
      currPairPosition++;
    } else if (canStepForwards()) { 
      SemanticPair currPair = pairHistory.get(currPairPosition),
                   nextPair = currPair.getSemanticInterpretation();

      String ruleApplied = nextPair.getDerivedBy();

      pairHistory.add(nextPair);
      ruleHistory.add(ruleApplied);
      currPairPosition++;
    }
  }

  public void stepBackwards() {
    if (canStepBackwards()) {
      currPairPosition--;
    }
  }

  public int getCurrPairPosition() {
    return currPairPosition;
  }

  public SemanticPair getSemanticPair() {
    return pairHistory.get(currPairPosition);
  }

  public String getRuleApplied() {
    return ruleHistory.get(currPairPosition);
  }

  public String getRuleAppliedAtPosition(int pos) {
    return ruleHistory.get(pos);
  }

  public void stepForwardsNTimes(int n) {
    for (int i = 0; i < n; i++) {
      stepForwards();
    }
  }

  public boolean canStepForwards() {
    return (currPairPosition < pairHistory.size() - 1)
            || !(pairHistory.get(currPairPosition).getProgram()
                 instanceof EmptyStmt);
  }

  public boolean canStepBackwards() {
    return currPairPosition > 0;
  }

  public StateTable getStateTable() {
    return pairHistory.get(currPairPosition).getStateTable();
  }

  public StateTable getStateTableAtPosition(int pos) {
    return pairHistory.get(pos).getStateTable();
  }

  public Stmt getExecutedWhileProgram() {
    return pairHistory.get(currPairPosition).getProgram();
  }

  public Stmt getExecutedWhileProgramAtPosition(int pos) {
    return pairHistory.get(pos).getProgram();
  }

  public WhileProgram getProgToInterpret() {
    return progToInterpret;
  }

  public String toString() {
    String ret = "";

    for (SemanticPair p : pairHistory) {
      ret += p.getProgram() + ", " + p.getStateTable() + "\n";
    }

    return ret;
  }
}
