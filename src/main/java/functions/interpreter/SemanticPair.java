package functions.interpreter;

import model.whiletree.Stmt;

public class SemanticPair {
  private Stmt program;
  private StateTable state;
  private String derivedBy = "";

  public SemanticPair(Stmt s, StateTable st) {
    program = s;
    state = new StateTable();
    //fresh copy
    state.updateFromOtherStateTable(st);
  }

  public SemanticPair(Stmt s, StateTable st, String d) {
    this(s, st);
    derivedBy = d;
  }

  public Stmt getProgram() {
    return program;
  }

  public StateTable getStateTable() {
    return state;
  }

  public String getDerivedBy() {
    return derivedBy;
  }

  public String toString() {
    return program + "\n\t" + state;
  }

  public SemanticPair getSemanticInterpretation() {
    return program.getSemanticInterpretation(state);
  }
}
