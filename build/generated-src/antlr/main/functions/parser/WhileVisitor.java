// Generated from functions/parser/While.g4 by ANTLR 4.5
package functions.parser;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link WhileParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface WhileVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link WhileParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(WhileParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link WhileParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(WhileParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by the {@code comp}
	 * labeled alternative in {@link WhileParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComp(WhileParser.CompContext ctx);
	/**
	 * Visit a parse tree produced by the {@code skip}
	 * labeled alternative in {@link WhileParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSkip(WhileParser.SkipContext ctx);
	/**
	 * Visit a parse tree produced by the {@code while}
	 * labeled alternative in {@link WhileParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile(WhileParser.WhileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code if}
	 * labeled alternative in {@link WhileParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf(WhileParser.IfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stmtbracket}
	 * labeled alternative in {@link WhileParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmtbracket(WhileParser.StmtbracketContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assign}
	 * labeled alternative in {@link WhileParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign(WhileParser.AssignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code not}
	 * labeled alternative in {@link WhileParser#bexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot(WhileParser.NotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bexpbracket}
	 * labeled alternative in {@link WhileParser#bexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBexpbracket(WhileParser.BexpbracketContext ctx);
	/**
	 * Visit a parse tree produced by the {@code and}
	 * labeled alternative in {@link WhileParser#bexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(WhileParser.AndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code equals}
	 * labeled alternative in {@link WhileParser#bexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEquals(WhileParser.EqualsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code true}
	 * labeled alternative in {@link WhileParser#bexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrue(WhileParser.TrueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code false}
	 * labeled alternative in {@link WhileParser#bexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFalse(WhileParser.FalseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code leq}
	 * labeled alternative in {@link WhileParser#bexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLeq(WhileParser.LeqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code add}
	 * labeled alternative in {@link WhileParser#aexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd(WhileParser.AddContext ctx);
	/**
	 * Visit a parse tree produced by the {@code subtract}
	 * labeled alternative in {@link WhileParser#aexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubtract(WhileParser.SubtractContext ctx);
	/**
	 * Visit a parse tree produced by the {@code variable}
	 * labeled alternative in {@link WhileParser#aexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(WhileParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numeral}
	 * labeled alternative in {@link WhileParser#aexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumeral(WhileParser.NumeralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiply}
	 * labeled alternative in {@link WhileParser#aexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiply(WhileParser.MultiplyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code axepbracket}
	 * labeled alternative in {@link WhileParser#aexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAxepbracket(WhileParser.AxepbracketContext ctx);
	/**
	 * Visit a parse tree produced by the {@code charvar}
	 * labeled alternative in {@link WhileParser#var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharvar(WhileParser.CharvarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code charnumvar}
	 * labeled alternative in {@link WhileParser#var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharnumvar(WhileParser.CharnumvarContext ctx);
}