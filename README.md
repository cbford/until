# until: A COMP11212 learning resource

### Building
Use [gradle](https://gradle.org/) to build the application. You will need to
create the build.gradle and a template is provided in build.gradle.template.

A 'fat jar' is automatically created containg all required additional libraries
(antlr, java-smt princess).

Some options that can be uncommented include enabling syntastic support in this
repository, increasing the smt solver's jvm memory allowance and adding a
gradle run flag for hidpi screen support.

### Running
Run the created jar file with `java -jar build/libs/until-all.jar`.
The `--hidpi` flag can be appended to increase font size on hidpi screens.
The `--extendedGrammar` flag can be appended to extend the allowed while syntax
